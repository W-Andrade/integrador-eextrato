# Summary

* [Integrador E-extrato](README.md)
* [Instalação](instalacao/README.md)
  * [Docker](instalacao/docker.md)
  * [Windows/Linux](instalacao/windows-linux.md)
  * [Configuração](instalacao/configuracao.md)
* [Guia de Usuário](login/README.md)
  * [Login](login/login.md)
  * [Vendas](login/vendas.md)
  * [Pagamentos](login/pagamentos.md)
  * [Ajustes e Tarifas](login/ajustes-e-tarifas.md)
  * [Usuários](login/usuarios.md)
  * [Lojas](login/lojas.md)
* [Integração ERP](integracao/README.md)
  * [Itec Brazil - Gestão](integracao/itec-brazil.md)

