# Vendas

Listar e consultar o status de conciliação das vendas.

### Filtro de Vendas

Campos dispníveis para filtro de pesquisa. O preenchimento do período de venda é obrigatório.

![](/assets/filtro_de_vendas.png)

Status de conciliação :

* Aguardando Envio
* Aguardando Retorno
* Falha no envio
* Falha na Conciliação
* Conciliado

### Lista de Vendas

![](/assets/filtro_vendas_resultado.png)

