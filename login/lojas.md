# Lojas

### Cadastro de Lojas

O cadastro de Lojas não é obrigatório, atualmente é utilizado apenas como opção de filtro em consultas.

Para adicionar uma nova Loja o preenchimento de todos os campos abaixo são obrigatórios.

![](/assets/loja_cadastro)

### Lista de Lojas

Todas as Lojas cadastradas.

Para alterar ou excluir uma Loja desejada clique no comando correspondente na coluna **Ação**

![](/assets/loja_cadastro.png)

