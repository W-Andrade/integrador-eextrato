# Usuários

### Cadastro de Usuários

Para adicionar um novo usuário o preenchimento de todos os campos abaixo são obrigatórios.

![](/assets/cadastro_usuario)

### Lista de Usuários

Todos os usuários cadastrados.

Para alterar ou excluir um usuário desejado clique no comando correspondente na coluna **Ação**.

![](/assets/usuarios_lista.png)

