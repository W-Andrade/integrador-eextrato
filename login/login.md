# Login

![](/assets/login.png)

Acesse o módulo de administração utilizando o navegador de Internet, informe na barra de endereços o nome ou endereço ip do servidor hospedeiro seguido da porta de comunicação.

Para acessar o sistema utilize um login de usuário e senha cadastrado.

O Login e Senha padrão de instalação são:

Login : admin

Senha: admin

**É altamente recomendado a alteração da senha após a instalação.**

