# Configuração

Parâmetros de configuração do sistema. O arquivo application.properties será criado no processo de instalação.

#### application.properties

| Parâmetro | Descrição |
| :--- | :--- |
| server.port | Número da porta para receber conexões Web. Default **8080** |
| server.contextPath | Nome da aplicação. Caso configurada deverá ser utilizada na url do navegador. Deve iniciar com / |
| eextrato.chaveAcesso | Chave de acesso sistema E-extrato |
| eextrato.chaveSecreta | Senha sistema E-extrato |
| db.default.host | Nome ou endereço IP do servidor de banco de dados |
| db.default.port | Porta de conexão do servidor de banco de dados |
| db.default.instance | Nome da instância de conexão do banco de dados. Caso informado não poderá ser informado o parametro **db.default.port** |
| db.default.base | Nome da base do banco de dados |
| db.default.user | Nome de usuário para login do banco de dados |
| db.default.pass | Senha para login do banco de dados |
| db.default.driver | Driver do banco de dados: mssql ou postgresql. Default **mssql** |



