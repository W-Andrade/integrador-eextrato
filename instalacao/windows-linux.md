# Windows/Linux

#### Requisitos de Sistema

* Sistema operacional Windows ou Linux
* Plataforma Java : Oracle JDK 1.8
* Memória : 2 GB +
* CPU: 2 GHZ +

#### Download

Faça o download do arquivo integrador-extrato-&lt;versao&gt;.jar através do link abaixo:

[Download da última versão](http://ci.convergencia.io/nexus/content/groups/public/convergencia/integrador-eextrato/)

#### Instalação

Execute o comando abaixo e configure os parâmetros solicitados. A documentação de cada parâmetro pode ser consultada na seção [Configuração](/instalacao/configuracao.md).

java -jar integrador-eextrato-&lt;versao&gt;.jar install

