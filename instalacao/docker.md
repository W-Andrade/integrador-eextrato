# Docker

Docker é uma plataforma open source para conteinerização de ambientes de software quer fornece uma camada adicional de abstração e automação de virtualização de sistema de nível operacional em Linux, Mac OS e Windows.

#### Requisitos de Sistema

* Sistema operacional Windows ou  Linux
* Plataforma Docker
* Memória : 2 GB +
* CPU: 2 GHZ +

#### docker pull

O comando docker pull é responsável pelo download da imagem. Para mais detalhes consulta a documentação de referência através [deste link](https://docs.docker.com/engine/reference/commandline/pull/).

Nome da imagem: **convergencia/integrador-eextrato**

O nome da imagem é formado pela estrutura: **nome\_organizacao/repositorio&lt;:versao&gt;, **omitindo-se a versão será realizado o download da última versão \(latest\).

```
C:\>docker pull convergencia/integrador-eextrato
Using default tag: latest
latest: Pulling from convergencia/integrador-eextrato
627beaf3eaaf: Already exists
95a531c0fa10: Already exists
b03e476748e7: Already exists
f07c46edac0b: Downloading [====>                                              ] 4.433 MB/48.54 MB
6051e78b4925: Downloading [====>                                              ] 3.932 MB/48.54 MB
f07c46edac0b: Pull complete
6051e78b4925: Pull complete
Digest: sha256:bba6915df144ad4cc3193d6e3ba157a3730992fbd0d2ae645a1ea845cd39ae86
Status: Downloaded newer image for convergencia/integrador-eextrato:latest
```

#### docker-compose

Composer é um utilitário para definir e executar aplicações Docker. Para maiores informações consulta a documentação de referência através [deste link](https://docs.docker.com/compose/).

#### Definindo o serviço

Para definir um serviço é necessário criar o arquivo docker-compopse.yml conforme exemplo abaixo.

docker-compose.yml

```yaml
version: '2'

services:

  e-extrato:
      image: convergencia/integrador-eextrato
      restart: always
      ports:
        - "8080:8080"
      container_name: integrador-eextrato
      hostname: integrador-eextrato
      environment:
        - eextrato.chaveAcesso=<informe o valor>
        - eextrato.chaveSecreta=<informe o valor>
        - db.default.host=<informe o valor>
        - db.default.base=<informe o valor>
        - db.default.user=<informe o valor>
        - db.default.pass=<informe o valor>
      logging:
        driver: json-file
        options:
          max-size: "50m"
          max-file: "5"
```

#### Criando e executando o container

O comando docker-compose irá realizar o download da imagem automaticamente caso ela não exista local. A utilização do docker-compose dispensa a utilização dos comandos docker-pull e docker-run para instalação e execução do container.

```
C:\>docker-compose up -d
Creating integrador-eextrato

C:\>docker ps -a
CONTAINER ID        IMAGE                              COMMAND                  CREATED             STATUS              PORTS                    NAMES
973ebdee197f        convergencia/integrador-eextrato   "sh -c 'java -Djav..."   6 seconds ago       Up 5 seconds        0.0.0.0:8080->8080/tcp   integrador-eextrato
```

Após a inicialização do container a aplicação deverá estar disponível para acesso web e operando normalmente.

